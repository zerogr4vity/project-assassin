#-------------------------------------------------
#
# Project created by QtCreator 2012-06-10T16:57:10
#
#-------------------------------------------------

QT       += core gui

TARGET = Project_Assassin
TEMPLATE = app


SOURCES += main.cpp\
        dialog.cpp \
    Assassin.cpp \
    Threadpool.cpp \
    Killer.cpp

HEADERS  += dialog.h \
    Assassin.h \
    Threadpool.h \
    Killer.h

FORMS    += dialog.ui
