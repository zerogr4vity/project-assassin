#include "Assassin.h"
#include <QtCore>
#include <cstdio>

Assassin::Assassin(QObject *parent) :
    QObject(parent)
{
    running = true;
}

Assassin::~Assassin()
{
    this->~QObject();
}

void Assassin::setup(QThread *cThread, process_list allowed)
{
    connect(cThread, SIGNAL(started()), this, SLOT(run()));
    processes = allowed;
}

void Assassin::run()
{
    process_list temp, new_procs;
    int key;

    emit showMessage("Listening...");
    forever
    {
        temp = EnumProcs();
        foreach(key, temp.keys())
        {
            if (!isIn(temp[key]))
            {
                new_procs.insert(key, temp[key]);
            }
        }
        if (!new_procs.isEmpty())
        {
            emit killProcesses(new_procs);
            new_procs.clear();
        }

        //check if loop should quit
        if(!running)
        {
            break;
        }
    }

    //tell user function has stopped
    emit showMessage("Stopped listening.");
    emit doneListening();
}

void Assassin::setStop(bool value)
{
    QMutex mutex;
    mutex.lock();
    running = value;
    mutex.unlock();
}

bool Assassin::getRunning()
{
    return running;
}

process_list Assassin::EnumProcs()
{
    QHash<int, QString> result;
    QString name;

    QString command1 = "ps -p ";
    QString command2 = " -o comm=";
    QString command;

    QByteArray temp;

    int key, counter = 0;
    char buffer[300];

    //create list of processes
    FILE *p = popen("ps -Ao pid", "r");

    //clear out column header
    if(fgets(buffer, 300, p))
    {
        //put process list into vector
        while(!feof(p))
        {
            if(fgets(buffer, 10, p))
            {
                //get PID
                key = atoi(buffer);

                //get process title
                command = command1 + QString::number(key) + command2;
                temp = command.toAscii();
                FILE *title = popen(temp.data(), "r");
                if(fgets(buffer, 300, title))
                {
                    counter = 0;
                    name.clear();
                    while(buffer[counter] != '\n')
                    {
                        name += buffer[counter++];
                    }
                }
                pclose(title);

                //append process to table
                result.insert(key, name);
            }
        }
    }

    //close process list
    pclose(p);

    return result;
}

bool Assassin::isIn(QString name)
{
    int key;
    foreach(key, processes.keys())
    {
        if(processes[key] == name)
        {
            return true;
        }
    }
    return false;
}
