#ifndef ASSASSIN_H
#define ASSASSIN_H

#include <QObject>
#include <QThread>
#include <QtCore>
#include <QHash>

typedef QHash<int, QString> process_list;

class Assassin : public QObject
{
    Q_OBJECT
public:
    explicit Assassin(QObject *parent = 0);
    ~Assassin();
    void setup(QThread *, process_list);
    bool getRunning();
    void setStop(bool);
    process_list EnumProcs();
    bool isIn(QString);
    
signals:
    void showMessage(QString);
    void killProcesses(process_list);
    void doneListening();
    
public slots:
    void run();

private:
    bool running;
    process_list processes;
};

#endif // ASSASSIN_H
