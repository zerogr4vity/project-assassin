#ifndef KILLER_H
#define KILLER_H

#include <QObject>
#include <QThread>
#include <QHash>

typedef QHash<int, QString> process_list;

class Killer : public QObject
{
    Q_OBJECT
public:
    explicit Killer(QObject *parent = 0);
    ~Killer();
    void setup(QThread *, process_list);
    
signals:
    void showMessage(QString);
    void killFinished();
    
public slots:
    void run();

private:
    process_list processes;
    
};

#endif // KILLER_H
