#include "Threadpool.h"

ThreadPool::ThreadPool()
{
    //initialize new threads
    for (int i = 0; i < POOLSIZE; i++)
    {
        pool[i] = new QThread;
    }

    //set starting position for queue
    next_thread = 0;
}

void ThreadPool::DequeueEnqueue()
{
    next_thread = (next_thread + 1) % POOLSIZE;
}

QThread *ThreadPool::getNextThread()
{
    while(pool[next_thread]->isRunning())
    {
        DequeueEnqueue();
    }
    return pool[next_thread];
}
