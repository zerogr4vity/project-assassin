#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QThread>
#include "Assassin.h"
#include "Threadpool.h"
#include "Killer.h"
#include <QHash>

typedef QHash<int, QString> process_list;

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

    Assassin *listener;
    
private slots:
    void on_ViewProcsButton_clicked();
    void on_showMessage(QString);
    void on_killProcesses(process_list);
    void on_stopListeningButton_clicked();

private:
    Ui::Dialog *ui;
    QThread *listenThread;
    process_list allowedProcs;
    ThreadPool assassins;
};

#endif // DIALOG_H
