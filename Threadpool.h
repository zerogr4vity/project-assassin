#ifndef THREADPOOL_H
#define THREADPOOL_H
#include <QThread>

#define POOLSIZE 10

class ThreadPool
{
public:
    ThreadPool();
    void DequeueEnqueue();
    QThread *getNextThread();

private:
    QThread* pool[POOLSIZE];
    int next_thread;
};

#endif // THREADPOOL_H
