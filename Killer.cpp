#include "Killer.h"
#include <sys/types.h>
#include <signal.h>
Killer::Killer(QObject *parent) :
    QObject(parent)
{
}

Killer::~Killer()
{
    this->~QObject();
}

void Killer::setup(QThread *cThread, process_list procs)
{
    connect(cThread, SIGNAL(started()), this, SLOT(run()));
    processes = procs;
}

void Killer::run()
{
    //kill processes
    int key;
    foreach(key, processes.keys())
    {
        kill(key, SIGKILL);

        //show kill message
        emit showMessage("Process terminated: " + QString::number(key) + processes[key]);
    }
    emit killFinished();
}
