#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    listener = new Assassin();
    connect(listener, SIGNAL(showMessage(QString)), this, SLOT(on_showMessage(QString)));
    connect(listener, SIGNAL(killProcesses(process_list)), this, SLOT(on_killProcesses(process_list)));

    allowedProcs = listener->EnumProcs();

    qRegisterMetaType<process_list>("process_list");

    //create thread
    listenThread = new QThread;

    //setup the connection
    listener->setup(listenThread, allowedProcs);
    connect(listener, SIGNAL(doneListening()), listenThread, SLOT(quit()));
    listener->moveToThread(listenThread);

    //start the thread
    listenThread->start();
}

Dialog::~Dialog()
{
    delete listener;
    delete ui;
}

void Dialog::on_ViewProcsButton_clicked()
{
    int key;

    //show processes in textbox
    ui->textEdit->append("ALLOWED PROCESSES:");
    foreach(key, allowedProcs.keys())
    {
        ui->textEdit->append(QString::number(key) + ": " + allowedProcs[key]);
    }
    ui->textEdit->append("\nNumber of processes allowed: " + QString::number(allowedProcs.size()));
}

void Dialog::on_showMessage(QString buffer)
{
    ui->textEdit->append(buffer);
}

void Dialog::on_killProcesses(process_list new_procs)
{
    //create kill object
    Killer *hitman = new Killer();

    //choose next empty thread from thread pool
    QThread *thread = assassins.getNextThread();

    //setup all connections
    hitman->setup(thread, new_procs);
    connect(hitman, SIGNAL(showMessage(QString)), this, SLOT(on_showMessage(QString)));
    connect(hitman, SIGNAL(killFinished()), thread, SLOT(quit()));

    //place killer on thread
    hitman->moveToThread(thread);

    //start thread
    thread->start();
}

void Dialog::on_stopListeningButton_clicked()
{
    listener->setStop(false);
}
